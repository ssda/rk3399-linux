#!/bin/bash

CMD=`realpath $0`
COMMON_DIR=`dirname $CMD`
TOP_DIR=$(realpath $COMMON_DIR/../../..)
BOARD_CONFIG=$TOP_DIR/device/rockchip/.BoardConfig.mk
source $BOARD_CONFIG
source $TOP_DIR/device/rockchip/common/Version.mk


if [ ! -n "$1" ];then
	echo "build all and save all as default"
	echo "============Start build allsave============"
        echo "RK_KERNEL_DTS=$RK_KERNEL_DTS"
	echo "RK_ROOTFS_IMG=$RK_ROOTFS_IMG"
        echo "========================================="
	BUILD_TARGET=allsave
else
	BUILD_TARGET="$1"
	NEW_BOARD_CONFIG=$TOP_DIR/device/rockchip/$RK_TARGET_PRODUCT/$1
fi



choose_board()
{
	echo "============You're building on Linux==========="
	echo "Please choose BoardConfig"
	echo "	1. BoardConfig_rd3399_buildroot"
	echo "	2. BoardConfig_rd3399_ubuntu_desktop"
	echo "	3. BoardConfig_rd3399_ubuntu_core"
	echo "	4. BoardConfig_rd3399_debian"
	echo "	5. BoardConfig_king3399_buildroot"
	echo "	6. BoardConfig_king3399_ubuntu_desktop"
	echo "	7. BoardConfig_king3399_ubuntu_core"
	echo "	8. BoardConfig_king3399_debian"
	echo "	9. BoardConfig_rp3399_buildroot"
	echo "	10.BoardConfig_rp3399_ubuntu_desktop"
	echo "	11.BoardConfig_rp3399_ubuntu_core"
	echo "	12.BoardConfig_rp3399_debian"
	echo "	13.BoardConfig_nano3399_buildroot"
	echo "	14.BoardConfig_nano3399_ubuntu_desktop"
	echo "	15.BoardConfig_nano3399_ubuntu_core"
	echo "	16.BoardConfig_nano3399_debian"
	echo -n "Please input num: "

	read board_num

	if [ $board_num -eq 1 ];then
		NEW_BOARD_CONFIG=$TOP_DIR/device/rockchip/rk3399/BoardConfig_rd3399_buildroot.mk
	elif [ $board_num -eq 2 ];then
		NEW_BOARD_CONFIG=$TOP_DIR/device/rockchip/rk3399/BoardConfig_rd3399_ubuntu.mk
        elif [ $board_num -eq 3 ];then
                NEW_BOARD_CONFIG=$TOP_DIR/device/rockchip/rk3399/BoardConfig_rd3399_ubuntu_core.mk
	elif [ $board_num -eq 4 ];then
		NEW_BOARD_CONFIG=$TOP_DIR/device/rockchip/rk3399/BoardConfig_rd3399_debian.mk
	elif [ $board_num -eq 5 ];then
		NEW_BOARD_CONFIG=$TOP_DIR/device/rockchip/rk3399/BoardConfig_king3399_buildroot.mk
	elif [ $board_num -eq 6 ];then
		NEW_BOARD_CONFIG=$TOP_DIR/device/rockchip/rk3399/BoardConfig_king3399_ubuntu.mk
        elif [ $board_num -eq 7 ];then
                NEW_BOARD_CONFIG=$TOP_DIR/device/rockchip/rk3399/BoardConfig_king3399_ubuntu_core.mk
        elif [ $board_num -eq 8 ];then
                NEW_BOARD_CONFIG=$TOP_DIR/device/rockchip/rk3399/BoardConfig_king3399_debian.mk
        elif [ $board_num -eq 9 ];then
                NEW_BOARD_CONFIG=$TOP_DIR/device/rockchip/rk3399/BoardConfig_rp3399_buildroot.mk
        elif [ $board_num -eq 10 ];then
                NEW_BOARD_CONFIG=$TOP_DIR/device/rockchip/rk3399/BoardConfig_rp3399_ubuntu.mk
        elif [ $board_num -eq 11 ];then
                NEW_BOARD_CONFIG=$TOP_DIR/device/rockchip/rk3399/BoardConfig_rp3399_ubuntu_core.mk
        elif [ $board_num -eq 12 ];then
                NEW_BOARD_CONFIG=$TOP_DIR/device/rockchip/rk3399/BoardConfig_rp3399_debian.mk
        elif [ $board_num -eq 13 ];then
                NEW_BOARD_CONFIG=$TOP_DIR/device/rockchip/rk3399/BoardConfig_nano3399_buildroot.mk
        elif [ $board_num -eq 14 ];then
                NEW_BOARD_CONFIG=$TOP_DIR/device/rockchip/rk3399/BoardConfig_nano3399_ubuntu.mk
        elif [ $board_num -eq 15 ];then
                NEW_BOARD_CONFIG=$TOP_DIR/device/rockchip/rk3399/BoardConfig_nano3399_ubuntu_core.mk
        elif [ $board_num -eq 16 ];then
                NEW_BOARD_CONFIG=$TOP_DIR/device/rockchip/rk3399/BoardConfig_nano3399_debian.mk
	else
    		echo "Can't found board config, please check again"
		exit 1
	fi
}

usage()
{
	echo "====USAGE: build.sh modules===="
	echo "uboot              -build uboot"
	echo "kernel             -build kernel"
	echo "modules            -build kernel modules"
	echo "rootfs             -build default rootfs, currently build buildroot as default"
	echo "buildroot          -build buildroot rootfs"
	echo "ramboot            -build ramboot image"
	echo "yocto              -build yocto rootfs, currently build ros as default"
	echo "ros                -build ros rootfs"
	echo "debian             -build debian rootfs"
	echo "pcba               -build pcba"
	echo "recovery           -build recovery"
	echo "all                -build uboot, kernel, rootfs, recovery image"
	echo "cleanall           -clean uboot, kernel, rootfs, recovery"
	echo "firmware           -pack all the image we need to boot up system"
	echo "updateimg          -pack update image"
	echo "otapackage         -pack ab update otapackage image"
	echo "save               -save images, patches, commands used to debug"
	echo "default            -build all modules"
}

function build_uboot(){
	# build uboot
	echo "============Start build uboot============"
	echo "TARGET_UBOOT_CONFIG=$RK_UBOOT_DEFCONFIG"
	echo "========================================="
	if [ -f u-boot/*_loader_*.bin ]; then
		rm u-boot/*_loader_*.bin
	fi
	cd u-boot && ./make.sh $RK_UBOOT_DEFCONFIG && cd -
	if [ $? -eq 0 ]; then
		echo "====Build uboot ok!===="
	else
		echo "====Build uboot failed!===="
		exit 1
	fi
}

function build_kernel(){
	# build kernel
	echo "============Start build kernel============"
	echo "TARGET_ARCH          =$RK_ARCH"
	echo "TARGET_KERNEL_CONFIG =$RK_KERNEL_DEFCONFIG"
	echo "TARGET_KERNEL_DTS    =$RK_KERNEL_DTS"
	echo "=========================================="
	cd $TOP_DIR/kernel && make ARCH=$RK_ARCH $RK_KERNEL_DTS.img -j$RK_JOBS && cd -
	if [ $? -eq 0 ]; then
		echo "====Build kernel ok!===="
	else
		echo "====Build kernel failed!===="
		exit 1
	fi
}

function build_modules(){
	echo "============Start build kernel modules============"
	echo "TARGET_ARCH          =$RK_ARCH"
	echo "TARGET_KERNEL_CONFIG =$RK_KERNEL_DEFCONFIG"
	echo "=================================================="
	cd $TOP_DIR/kernel && make ARCH=$RK_ARCH $RK_KERNEL_DEFCONFIG && make ARCH=$RK_ARCH modules -j$RK_JOBS && cd -
	if [ $? -eq 0 ]; then
		echo "====Build kernel ok!===="
	else
		echo "====Build kernel failed!===="
		exit 1
	fi
}

function build_buildroot(){
	# build buildroot
	echo "==========Start build buildroot=========="
	echo "TARGET_BUILDROOT_CONFIG=$RK_CFG_BUILDROOT"
	echo "========================================="
	/usr/bin/time -f "you take %E to build builroot" $COMMON_DIR/mk-buildroot.sh $BOARD_CONFIG
	if [ $? -eq 0 ]; then
		echo "====Build buildroot ok!===="
	else
		echo "====Build buildroot failed!===="
		exit 1
	fi
}

function build_ramboot(){
	# build ramboot image
        echo "=========Start build ramboot========="
        echo "TARGET_RAMBOOT_CONFIG=$RK_CFG_RAMBOOT"
        echo "====================================="
	/usr/bin/time -f "you take %E to build ramboot" $COMMON_DIR/mk-ramdisk.sh ramboot.img $RK_CFG_RAMBOOT
	if [ $? -eq 0 ]; then
		echo "====Build ramboot ok!===="
	else
		echo "====Build ramboot failed!===="
		exit 1
	fi
}

function build_rootfs(){
	build_buildroot
}

function build_ros(){
	build_buildroot
}

function build_yocto(){
	echo "we don't support yocto at this time"
}

function build_debian(){
        # build debian
        echo "===========Start build debian==========="
	echo "TARGET_ARCH=$RK_ARCH"
        echo "RK_DISTRO_DEFCONFIG=$RK_DISTRO_DEFCONFIG"
	echo "========================================"
	cd distro
	make ARCH=$RK_ARCH &RK_DISTRO_DEFCONFIG
	cd ..
	/usr/bin/time -f "you take %E to build debian" $TOP_DIR/distro/make.sh $RK_DISTRO_DEFCONFIG $RK_ARCH
        if [ $? -eq 0 ]; then
                echo "====Build debian ok!===="
        else
                echo "====Build debian failed!===="
                exit 1
        fi
}

function build_recovery(){
	# build recovery
	echo "==========Start build recovery=========="
	echo "TARGET_RECOVERY_CONFIG=$RK_CFG_RECOVERY"
	echo "========================================"
	/usr/bin/time -f "you take %E to build recovery" $COMMON_DIR/mk-ramdisk.sh recovery.img $RK_CFG_RECOVERY
	if [ $? -eq 0 ]; then
		echo "====Build recovery ok!===="
	else
		echo "====Build recovery failed!===="
		exit 1
	fi
}

function build_pcba(){
	# build pcba
	echo "==========Start build pcba=========="
	echo "TARGET_PCBA_CONFIG=$RK_CFG_PCBA"
	echo "===================================="
	/usr/bin/time -f "you take %E to build pcba" $COMMON_DIR/mk-ramdisk.sh pcba.img $RK_CFG_PCBA
	if [ $? -eq 0 ]; then
		echo "====Build pcba ok!===="
	else
		echo "====Build pcba failed!===="
		exit 1
	fi
}

function build_all(){
	echo "============================================"
	echo "TARGET_ARCH=$RK_ARCH"
	echo "TARGET_PLATFORM=$RK_TARGET_PRODUCT"
	echo "TARGET_UBOOT_CONFIG=$RK_UBOOT_DEFCONFIG"
	echo "TARGET_KERNEL_CONFIG=$RK_KERNEL_DEFCONFIG"
	echo "TARGET_KERNEL_DTS=$RK_KERNEL_DTS"
	echo "TARGET_BUILDROOT_CONFIG=$RK_CFG_BUILDROOT"
	echo "TARGET_RECOVERY_CONFIG=$RK_CFG_RECOVERY"
	echo "TARGET_PCBA_CONFIG=$RK_CFG_PCBA"
	echo "TARGET_RAMBOOT_CONFIG=$RK_CFG_RAMBOOT"
	echo "============================================"
	build_uboot
	build_kernel
#	build_rootfs
	build_recovery
	build_ramboot
}

function clean_all(){
	echo "clean uboot, kernel, rootfs, recovery"
	cd $TOP_DIR/u-boot/ && make distclean && cd -
	cd $TOP_DIR/kernel && make distclean && cd -
	rm -rf buildroot/out
}

function build_firmware(){
	# mkfirmware.sh to genarate image
	./mkfirmware.sh $BOARD_CONFIG
	if [ $? -eq 0 ]; then
	    echo "Make image ok!"
	else
	    echo "Make image failed!"
	    exit 1
	fi
}

function build_updateimg(){
	IMAGE_PATH=$TOP_DIR/rockdev
	PACK_TOOL_DIR=$TOP_DIR/tools/linux/Linux_Pack_Firmware
    if [ "$RK_LINUX_AB_ENABLE"x = "true"x ];then
        echo "Make Linux a/b update.img."
	    build_ota_ab_updateimg
        source_package_file_name=`ls -lh $PACK_TOOL_DIR/rockdev/package-file | awk -F ' ' '{print $NF}'`
        cd $PACK_TOOL_DIR/rockdev && ln -fs "$source_package_file_name"-ab package-file && ./mkupdate.sh && cd -
        mv $PACK_TOOL_DIR/rockdev/update.img $IMAGE_PATH/update_ab.img
        cd $PACK_TOOL_DIR/rockdev && ln -fs $source_package_file_name package-file && cd -
        if [ $? -eq 0 ]; then
            echo "Make Linux a/b update image ok!"
        else
            echo "Make Linux a/b update image failed!"
            exit 1
        fi

    else
	TIME=`date +%Y%m%d-%H%M%S`
        echo "Make update.img"
        cd $PACK_TOOL_DIR/rockdev && ./mkupdate.sh && cd -
        #mv $PACK_TOOL_DIR/rockdev/update.img $IMAGE_PATH
	mv $PACK_TOOL_DIR/rockdev/update.img $IMAGE_PATH/update-$RP_BOARD-$RP_SYSTEM-$TIME.img
        if [ $? -eq 0 ]; then
            echo "Make update image ok!"
        else
            echo "Make update image failed!"
            exit 1
        fi
    fi
}

function build_ota_ab_updateimg(){
    IMAGE_PATH=$TOP_DIR/rockdev
    PACK_TOOL_DIR=$TOP_DIR/tools/linux/Linux_Pack_Firmware

    echo "Make ota ab update.img"
    source_package_file_name=`ls -lh $PACK_TOOL_DIR/rockdev/package-file | awk -F ' ' '{print $NF}'`
    cd $PACK_TOOL_DIR/rockdev && ln -fs "$source_package_file_name"-ota package-file && ./mkupdate.sh && cd -
    mv $PACK_TOOL_DIR/rockdev/update.img $IMAGE_PATH/update_ota.img
    cd $PACK_TOOL_DIR/rockdev && ln -fs $source_package_file_name package-file && cd -
    if [ $? -eq 0 ]; then
        echo "Make update ota ab image ok!"
    else
        echo "Make update ota ab image failed!"
        exit 1
    fi
}

function build_save(){
	IMAGE_PATH=$TOP_DIR/rockdev
	DATE=$(date  +%Y%m%d.%H%M)
	STUB_PATH=Image/"$RK_KERNEL_DTS"_"$DATE"_RELEASE_TEST
	STUB_PATH="$(echo $STUB_PATH | tr '[:lower:]' '[:upper:]')"
	export STUB_PATH=$TOP_DIR/$STUB_PATH
	export STUB_PATCH_PATH=$STUB_PATH/PATCHES
	mkdir -p $STUB_PATH

	#Copy stubs
	mkdir -p $STUB_PATCH_PATH/kernel
	cp $TOP_DIR/kernel/.config $STUB_PATCH_PATH/kernel
	cp $TOP_DIR/kernel/vmlinux $STUB_PATCH_PATH/kernel
#	mkdir -p $STUB_PATH/IMAGES/
#	cp $IMAGE_PATH/* $STUB_PATH/IMAGES/

	#Save build command info
	echo "UBOOT:  defconfig: $RK_UBOOT_DEFCONFIG" >> $STUB_PATH/build_cmd_info
	echo "KERNEL: defconfig: $RK_KERNEL_DEFCONFIG, dts: $RK_KERNEL_DTS" >> $STUB_PATH/build_cmd_info
	echo "BUILDROOT: $RK_CFG_BUILDROOT" >> $STUB_PATH/build_cmd_info

}

function build_all_save(){
	build_all
	build_firmware
	build_updateimg
#	build_save
}
#=========================
# build target
#=========================
if [ $BUILD_TARGET == uboot ];then
    build_uboot
    exit 0
elif [ $BUILD_TARGET == kernel ];then
    build_kernel
    exit 0
elif [ $BUILD_TARGET == modules ];then
    build_modules
    exit 0
elif [ $BUILD_TARGET == rootfs ];then
    build_rootfs
    exit 0
elif [ $BUILD_TARGET == buildroot ];then
    build_buildroot
    exit 0
elif [ $BUILD_TARGET == recovery ];then
    build_kernel
    build_recovery
    exit 0
elif [ $BUILD_TARGET == ramboot ];then
    build_ramboot
    exit 0
elif [ $BUILD_TARGET == pcba ];then
    build_pcba
    exit 0
elif [ $BUILD_TARGET == yocto ];then
    build_yocto
    exit 0
elif [ $BUILD_TARGET == ros ];then
    build_ros
    exit 0
elif [ $BUILD_TARGET == debian ];then
    build_debian
    exit 0
elif [ $BUILD_TARGET == updateimg ];then
    build_updateimg
    exit 0
elif [ $BUILD_TARGET == otapackage ];then
    build_ota_ab_updateimg
    exit 0
elif [ $BUILD_TARGET == all ];then
    build_all
    exit 0
elif [ $BUILD_TARGET == firmware ];then
    build_firmware
    exit 0
elif [ $BUILD_TARGET == save ];then
    build_save
    exit 0
elif [ $BUILD_TARGET == cleanall ];then
    clean_all
    exit 0
elif [ $BUILD_TARGET == --help ] || [ $BUILD_TARGET == help ] || [ $BUILD_TARGET == -h ];then
    usage
    exit 0
elif [ $BUILD_TARGET == allsave ];then
    build_all_save
    exit 0
elif [ $BUILD_TARGET == init ];then
	choose_board
    rm -f $BOARD_CONFIG
    ln -s $NEW_BOARD_CONFIG $BOARD_CONFIG
	exit 0
elif [ -f $NEW_BOARD_CONFIG ];then
    rm -f $BOARD_CONFIG
    ln -s $NEW_BOARD_CONFIG $BOARD_CONFIG
else
    echo "Can't found build config, please check again"
    usage
    exit 1
fi

